f = open("citeseerx_alldata.xml","r")
g = open("data.xml","w")

line = f.readline()
g.write(line)
g.write("<all_data>\n")

while True:
    line = f.readline()
    if line == '':
        break
    if not line.startswith("<?xml"):
        g.write(line)

g.write("</all_data>\n")
f.close()
g.close()
