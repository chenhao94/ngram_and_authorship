from xml.etree import ElementTree as ET
from parser_utils import *
import re
import hyperdex.admin
import hyperdex.client

node_map = {}
out = open("authors_bulk.xml","w")
hd_out = open("hyperdex_log_authors.txt", "w")
c = hyperdex.client.Client(server_addr, hyperdex_coord)
lst = []
article_id = 0
author_id = 0
edge_id = 0

def set_up_author_edge_space():
    a = hyperdex.admin.Admin(server_addr, hyperdex_coord)

    a.rm_space("author_edges")
    a.add_space(" \
            space author_edges \
            key namepair \
            attributes \
                set(int) doc_id \
    ")

def addNode(name):
    global author_id
    if name not in node_map:
        author_id = author_id + 1
        node_map[name] = author_id
        out.write("<node id=\"%s\"></node>\n" % (name))

def addEdge(a1, a2):
    global edge_id
    global article_id
    addNode(a1)
    addNode(a2)
    namepair = makePair(a1, a2)
    if c.get('author_edges', namepair) is None:
        edge_id = edge_id + 1
        out.write("<edge id=\"%d\" source=\"%s\" target=\"%s\"><data key=\"confidence\">0.1</data><data key=\"doc_id\">%d</data></edge>\n"
            % (edge_id, a1, a2, article_id))
        c.put('author_edges',namepair,{'doc_id':{article_id}})
        hd_out.write("add %s %d\n" % (namepair, article_id))
    else:
        c.async_set_add('author_edges',namepair,{'doc_id':article_id})
        hd_out.write("addset %s %d\n" % (namepair, article_id))

    

def add_authors(aid, title):
    for name1 in lst:
        for name2 in lst:
            if name1 != name2:
                e = addEdge(name1, name2)

set_up_author_edge_space()

for event, item in parser:
    if item.tag == "{%s}title" % dc_ns and item.text is not None:
        title = check(item.text)
    elif item.tag == "{%s}creator" % dc_ns and item.text is not None:
        name = check(item.text)
        if name in lst or len(name) < 1:
            continue
        lst.append(name)
    elif item.tag == "{%s}identifier" % dc_ns and item.text is not None:
        if legal_title(title):
            article_id = article_id + 1
            add_authors(article_id, title)
        lst = []
        title=""
    item.clear()

out.close()
