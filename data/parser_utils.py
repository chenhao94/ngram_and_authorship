import re
from sets import Set
from xml.etree import ElementTree as ET

parser = ET.iterparse("data.xml")
ns = "http://www.openarchives.org/OAI/2.0/"
dc_ns = "http://purl.org/dc/elements/1.1/"
server_addr = '128.84.167.207'
hyperdex_coord = 7982
weaver_coord = 2002
weaver_config = '/local/ch94/ngram_and_authorship/weaver.yaml'

def check(str):
    return str.replace('$',' ').strip().encode('utf-8')

def legal_title(string):
    return len(re.findall('\w+', string))>1

def makePair(s1, s2):
    return s1+"$$"+s2

