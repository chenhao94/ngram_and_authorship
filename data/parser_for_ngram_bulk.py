from xml.etree import ElementTree as ET
import re
from sets import Set
from parser_utils import *
from utils import pair_to_int
from utils import int_to_pair
import hyperdex.admin
import hyperdex.client

out = open("ngrams_bulk.xml","w")
hd_out = open("hyperdex_log_ngrams.txt","w")
c = hyperdex.client.Client(server_addr, hyperdex_coord)
article_id = 0
edge_id = 0
word_set = Set()
authors = "$$"

filter_list = set(['a', 'an', 'the', 'this', 'that', 'these', 'those', 'in', 'on',
    'at', 'under', 'above', 'below', 'for',
    'to', 'of', 'by', 'then', 'is', 'are', 'am', 'be', 'when', 'what', 'why',
    'how', 'where', 'and', 'or', 'not', 'no', 'yes'])

def set_up_ngram_edge_space():
    a = hyperdex.admin.Admin(server_addr, hyperdex_coord)

    a.rm_space("ngram_edges")
    a.add_space(" \
            space ngram_edges \
            key wordpair \
            attributes \
                set(int) aid_pos_pair \
    ")

def addNode(word):
    global word_set
    if word not in word_set:
        out.write("<node id=\"%s\"></node>\n" % word)
        word_set.add(word)

def addEdge(word1, word2, aid, pos):
    global edge_id
    addNode(word2) # word1 has been added before
    wordpair = makePair(word1, word2)
    intpair = pair_to_int(aid, pos)
    if c.get('ngram_edges', wordpair) is None:
        edge_id = edge_id + 1
        out.write("<edge id=\"%d\" source=\"%s\" target=\"%s\"></edge>\n" %
                (edge_id, word1, word2))
        # need to store aid somewhere
        c.put('ngram_edges', wordpair, {'aid_pos_pair':{intpair}})
        hd_out.write("add %s %d\n" % (wordpair, intpair))
    else:
        c.async_set_add('ngram_edges', wordpair, {'aid_pos_pair':intpair})
        hd_out.write("addset %s %d\n" % (wordpair, intpair))


def print_words(out, string, aid, title):
    lst = re.findall('\w+', string.lower())
    last = ""
    pos = 0
    for word in lst:
        if word in filter_list:
            continue
        elif last != "":
            addEdge(last, word, aid, pos)
        else:
            addNode(word)
        last = word
        pos = pos + 1
    if last != "":
        addEdge(last, "a", aid, -1) # the last word need a outdegree

set_up_ngram_edge_space()
addNode("a")

for event, item in parser:
    if item.tag == "{%s}title" % dc_ns and item.text is not None:
        title = check(item.text)
    elif item.tag == "{%s}description" % dc_ns and item.text is not None:
        abstract = check(item.text.replace('$',' '))
    elif item.tag == "{%s}identifier" % dc_ns and item.text is not None:
        if legal_title(title):
            article_id = article_id + 1
            if len(abstract) > 0:
                print_words(out, abstract, article_id, title)
        title = abstract = ""
    item.clear()

out.close()
