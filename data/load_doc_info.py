# This is used for load `doc_info.txt` to Hyperdex

import hyperdex.admin
import hyperdex.client
import parser_utils

class DocInfo:
    def __init__(self, title, date, url, authors):
        self.title = title
        self.date = date
        self.url = url
        self.authors=[]
        for author in authors:
            self.authors.append(author)

def clean_up_space():
    a = hyperdex.admin.Admin(server_addr, hyperdex_coord)
    a.rm_space('docs')

def set_up_space():
    a = hyperdex.admin.Admin(server_addr, hyperdex_coord)

    a.add_space(" \
            space docs \
            key int doc_id \
            attributes \
                string title, \
                string date, \
                string url, \
                set(string) author \
    ")

def load_docs_info():
    c = hyperdex.client.Client(server_addr, hyperdex_coord)

    docs=set()
    aid = 0
    with open("doc_info.txt","r") as f:
        for line in f:
            lst = line.strip().split('$$')
            aid = aid + 1
            docs.add(c.async_put('docs', aid, 
                {'title':lst[0], 'date':lst[1], 'url':lst[2]}))
            for i in range(3, len(lst)-1):
                docs.add(c.async_set_add('docs', aid, {'author':lst[i]}))

    while docs:
        d = c.loop()
        docs.remove(d)
        d.wait()

def find_doc(aid):
    c = hyperdex.client.Client(server_addr, hyperdex_coord)
    doc = c.get("docs", aid);
    return DocInfo(doc['title'], doc['date'], doc['url'], doc['author'])

# set_up_space()
# load_docs_info()
