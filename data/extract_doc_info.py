from xml.etree import ElementTree as ET
from parser_utils import *
from utils import convert_str_to_date
import re
from datetime import date

out = open("doc_info.tmp","w")
article_id = 0
authors = "$$"

lst = []
docdate = None
datestr = ""

for event, item in parser:
    if item.tag == "{%s}date" % dc_ns and item.text is not None:
        try:
            newdate = convert_str_to_date(check(item.text))
        except Exception as e:
            pass
            #print "Warning: date convertion exception: " + str(e)
        if docdate is None or docdate > newdate:
            docdate = newdate
    elif item.tag == "{%s}title" % dc_ns and item.text is not None:
        title = check(item.text)
    elif item.tag == "{%s}creator" % dc_ns and item.text is not None:
        name = check(item.text)
        if name in lst or len(name) < 1:
            continue
        authors = authors + name + "$$"
        lst.append(name)
    elif item.tag == "{%s}identifier" % dc_ns and item.text is not None:
        url = check(item.text)
        if legal_title(title):
            article_id = article_id + 1
            if docdate is None:
                raise Exception("no date in this doc: %d - %s - %s" % (article_id, title, url))
            else:
                datestr = str(docdate).encode('utf-8')
            out.write(("%s$$%s$$%s%s\n" % (title, datestr, url, authors)))
        else:
            print url
        lst = []
        authors = "$$"
        url=title=""
        docdate = None
    item.clear()
