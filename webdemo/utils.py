from datetime import date
from datetime import timedelta
from numpy import histogram
import hyperdex.client as hclient
import weaver.client as wclient
import json
from sys import maxsize
from load_doc_info import find_doc
import re
import memcache

std_day = date(2000,1,1)
c = wclient.Client('128.84.167.207',2002, '/local/ch94/weaver.yaml')
mc = memcache.Client(['127.0.0.1:11211'], debug=0)

def convert_str_to_date(string):
    mm = dd = 1
    match = re.match("(\d{4})-(\d{1,2})-(\d{1,2})", string)
    if match:
        dd = int(match.group(3))
    else:
        match = re.match("(\d{4})-(\d{1,2})", string)
    if match:
        mm = int(match.group(2))
    else:
        match = re.match("(\d{4})", string)
    if match:
        yy = int(match.group(1))
    else:
        raise Exception(string)
    return date(yy,mm,dd)

def date_to_int(string):
    global std_day
    return (convert_str_to_date(string) - std_day).days

def date_between(now, start, end):
    numnow = date_to_int(now)
    return date_to_int(start) <= numnow and numnow < date_to_int(end)

def list_to_key(lst):
    return '_'.join(lst).encode('utf-8')

def n_gram_histogram(words, start, end, bins=20):
    global c
    global mc
    min_day = date_to_int(start)
    max_day = date_to_int(end)

    res = map(lambda doc_id: [doc_id, find_doc(doc_id)], c.n_gram_path(words))
    histo_array = [ date_to_int(doc.date) for [doc_id,doc] in res]

    histo, edges = histogram(histo_array, bins)
    histo = histo.tolist()
    mc.set(list_to_key(words), res)
    edges = map(lambda num:str(std_day+timedelta(num)), edges)
    return [histo,edges]

def get_docs(words, start, end):
    global c
    global mc
    res = mc.get(list_to_key(words))
    lst = []
    if res is None: # cache miss
        res = map(lambda doc_id: [doc_id, find_doc(doc_id)], c.n_gram_path(words))
        mc.set(list_to_key(words), res)
    lst = filter(lambda x: date_between(x[1].date, start, end), res) 
    return [{"title":doc.title, "date":doc.date} for [doc_id, doc] in lst]
