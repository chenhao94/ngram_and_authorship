from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, make_response
from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired
import hyperdex.client as hclient
import weaver.client as wclient
import uuid
import json
import utils

#config
DEBUG = True
#create a simple application
app = Flask(__name__)
app.config.from_object(__name__)
# set the secret key Flask uses for cookie authentication. keep this really secret:
app.secret_key = '!@RVEERY%UK:jg.mdnsNGF@TV@Y%B@WCXDqxFV@'
#c = hclient.Client('127.0.0.1', 7982)

@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html");

@app.route('/n_gram_query', methods=['POST'])
def n_gram_query():
    words = json.loads(request.form['words'])
    return json.dumps(utils.n_gram_histogram(words,"1990-01-01","2009-01-01"))

@app.route('/memcache', methods=['POST'])
def memcache():
    words = json.loads(request.form['words'])
    return json.dumps(utils.get_docs(words, "2004-01-01","2005-01-01"))

if __name__ == '__main__':
     app.run(host='0.0.0.0',port=8080)
