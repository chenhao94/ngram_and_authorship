import requests
import json

lst = ['distributed','system']

r = requests.post('http://si028.systems.cs.cornell.edu:8080/n_gram_query', data={'words':json.dumps(lst)})
print r.text

r = requests.post('http://si028.systems.cs.cornell.edu:8080/memcache', data={'words':json.dumps(lst)})
print r.text
